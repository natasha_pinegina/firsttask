﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace First_tesk_baza_date
{
    public partial class theEntryintheDirectory : Form
    {
        public theEntryintheDirectory()
        {
            InitializeComponent();
        }

        private void button_Ok_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {

            DialogResult = DialogResult.Cancel;
            Close();
        }
        public Dictionary<int, string> PersonPinegina
        {
            set
            {
                comboBox_person.DataSource = value.ToArray();
                comboBox_person.DisplayMember = "Value";
            }
        }
        public int PersonId
        {
            get
            {
                return ((KeyValuePair<int, string>)comboBox_person.SelectedItem).Key;
            }
            set
            {
                int indx = 0;
                foreach (KeyValuePair<int, string> item in comboBox_person.Items)
                {
                    if (item.Key == value)
                    {
                        break;
                    }
                    indx++;
                }
                comboBox_person.SelectedIndex = indx;
            }
        }
        public Dictionary<int, string> PhonePinegina
        {
            set
            {
                comboBox_phohe.DataSource = value.ToArray();
                comboBox_phohe.DisplayMember = "Value";
            }
        }
        public int PhoneId
        {
            get
            {
                return ((KeyValuePair<int, string>)comboBox_phohe.SelectedItem).Key;
            }
            set
            {
                int indx = 0;
                foreach (KeyValuePair<int, string> item in comboBox_phohe.Items)
                {
                    if (item.Key == value)
                    {
                        break;
                    }
                    indx++;
                }
                comboBox_phohe.SelectedIndex = indx;
            }
        }
    }
}
