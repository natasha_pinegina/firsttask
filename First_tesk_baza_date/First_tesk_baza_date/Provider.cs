﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace First_tesk_baza_date
{
    public partial class Provider : Form
    {
        public Provider()
        {
            InitializeComponent();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        public Dictionary<int, int> ProviderPinegina
        {
            set
            {
                comboBox_score.DataSource = value.ToArray();
                comboBox_score.DisplayMember = "Value";
            }
        }
        public int ProviderId
        {
            get
            {
                return ((KeyValuePair<int, int>)comboBox_score.SelectedItem).Value;
            }
            set
            {
                int indx = 0;
                foreach (KeyValuePair<int, int> item in comboBox_score.Items)
                {
                    if (item.Value == value)
                    {
                        break;
                    }
                    indx++;
                }
                comboBox_score.SelectedIndex = indx;
            }
        }
    }
}
