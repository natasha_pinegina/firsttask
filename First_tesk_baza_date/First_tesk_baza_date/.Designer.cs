﻿namespace First_tesk_baza_date
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.main_TableControl = new System.Windows.Forms.TabControl();
            this.Abonent_tab = new System.Windows.Forms.TabPage();
            this.Abonent_date_Grid = new System.Windows.Forms.DataGridView();
            this.button_add = new System.Windows.Forms.Button();
            this.edit_buttom = new System.Windows.Forms.Button();
            this.Delete_buttom = new System.Windows.Forms.Button();
            this.contact_table = new System.Windows.Forms.TabPage();
            this.button_delete_contact = new System.Windows.Forms.Button();
            this.Contact_date_Grid = new System.Windows.Forms.DataGridView();
            this.button_edit_contact = new System.Windows.Forms.Button();
            this.buttom_add_contact = new System.Windows.Forms.Button();
            this.provider_table = new System.Windows.Forms.TabPage();
            this.button_add_provider = new System.Windows.Forms.Button();
            this.button_delete_provider = new System.Windows.Forms.Button();
            this.Provider_date_Grid = new System.Windows.Forms.DataGridView();
            this.Telephone_table = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.button_editClient = new System.Windows.Forms.Button();
            this.button_addClient = new System.Windows.Forms.Button();
            this.Phone_number_date_Grid = new System.Windows.Forms.DataGridView();
            this.Number_phone = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.FIO = new System.Windows.Forms.TextBox();
            this.ФИО = new System.Windows.Forms.Label();
            this.main_TableControl.SuspendLayout();
            this.Abonent_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Abonent_date_Grid)).BeginInit();
            this.contact_table.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Contact_date_Grid)).BeginInit();
            this.provider_table.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Provider_date_Grid)).BeginInit();
            this.Telephone_table.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Phone_number_date_Grid)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1010, 654);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 35);
            this.button1.TabIndex = 0;
            this.button1.Text = "Кнопка";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // main_TableControl
            // 
            this.main_TableControl.Controls.Add(this.Abonent_tab);
            this.main_TableControl.Controls.Add(this.contact_table);
            this.main_TableControl.Controls.Add(this.provider_table);
            this.main_TableControl.Controls.Add(this.Telephone_table);
            this.main_TableControl.Location = new System.Drawing.Point(30, 20);
            this.main_TableControl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.main_TableControl.Name = "main_TableControl";
            this.main_TableControl.SelectedIndex = 0;
            this.main_TableControl.Size = new System.Drawing.Size(1254, 608);
            this.main_TableControl.TabIndex = 1;
            // 
            // Abonent_tab
            // 
            this.Abonent_tab.BackColor = System.Drawing.Color.MediumPurple;
            this.Abonent_tab.Controls.Add(this.Abonent_date_Grid);
            this.Abonent_tab.Controls.Add(this.button_add);
            this.Abonent_tab.Controls.Add(this.edit_buttom);
            this.Abonent_tab.Controls.Add(this.Delete_buttom);
            this.Abonent_tab.Location = new System.Drawing.Point(4, 29);
            this.Abonent_tab.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Abonent_tab.Name = "Abonent_tab";
            this.Abonent_tab.Size = new System.Drawing.Size(1246, 575);
            this.Abonent_tab.TabIndex = 3;
            this.Abonent_tab.Text = "Абонент";
            // 
            // Abonent_date_Grid
            // 
            this.Abonent_date_Grid.BackgroundColor = System.Drawing.Color.MediumPurple;
            this.Abonent_date_Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Abonent_date_Grid.Location = new System.Drawing.Point(10, 11);
            this.Abonent_date_Grid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Abonent_date_Grid.Name = "Abonent_date_Grid";
            this.Abonent_date_Grid.RowHeadersWidth = 62;
            this.Abonent_date_Grid.Size = new System.Drawing.Size(833, 559);
            this.Abonent_date_Grid.TabIndex = 0;
            // 
            // button_add
            // 
            this.button_add.Location = new System.Drawing.Point(851, 34);
            this.button_add.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button_add.Name = "button_add";
            this.button_add.Size = new System.Drawing.Size(167, 35);
            this.button_add.TabIndex = 6;
            this.button_add.Text = "добавить абонента";
            this.button_add.UseVisualStyleBackColor = true;
            this.button_add.Click += new System.EventHandler(this.button_add_Click);
            // 
            // edit_buttom
            // 
            this.edit_buttom.Location = new System.Drawing.Point(851, 79);
            this.edit_buttom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.edit_buttom.Name = "edit_buttom";
            this.edit_buttom.Size = new System.Drawing.Size(167, 35);
            this.edit_buttom.TabIndex = 7;
            this.edit_buttom.Text = "изменить абонента";
            this.edit_buttom.UseVisualStyleBackColor = true;
            this.edit_buttom.Click += new System.EventHandler(this.edit_buttom_Click);
            // 
            // Delete_buttom
            // 
            this.Delete_buttom.Location = new System.Drawing.Point(851, 124);
            this.Delete_buttom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Delete_buttom.Name = "Delete_buttom";
            this.Delete_buttom.Size = new System.Drawing.Size(167, 35);
            this.Delete_buttom.TabIndex = 8;
            this.Delete_buttom.Text = "Удалить абонента";
            this.Delete_buttom.UseVisualStyleBackColor = true;
            this.Delete_buttom.Click += new System.EventHandler(this.Delete_buttom_Click);
            // 
            // contact_table
            // 
            this.contact_table.BackColor = System.Drawing.Color.Aquamarine;
            this.contact_table.Controls.Add(this.button_delete_contact);
            this.contact_table.Controls.Add(this.Contact_date_Grid);
            this.contact_table.Controls.Add(this.button_edit_contact);
            this.contact_table.Controls.Add(this.buttom_add_contact);
            this.contact_table.Location = new System.Drawing.Point(4, 29);
            this.contact_table.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.contact_table.Name = "contact_table";
            this.contact_table.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.contact_table.Size = new System.Drawing.Size(1246, 575);
            this.contact_table.TabIndex = 0;
            this.contact_table.Text = "Контакт";
            // 
            // button_delete_contact
            // 
            this.button_delete_contact.Location = new System.Drawing.Point(631, 100);
            this.button_delete_contact.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button_delete_contact.Name = "button_delete_contact";
            this.button_delete_contact.Size = new System.Drawing.Size(167, 35);
            this.button_delete_contact.TabIndex = 11;
            this.button_delete_contact.Text = "Удалить контакт";
            this.button_delete_contact.UseVisualStyleBackColor = true;
            this.button_delete_contact.Click += new System.EventHandler(this.button_delete_contact_Click);
            // 
            // Contact_date_Grid
            // 
            this.Contact_date_Grid.BackgroundColor = System.Drawing.Color.Aquamarine;
            this.Contact_date_Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Contact_date_Grid.Location = new System.Drawing.Point(12, 6);
            this.Contact_date_Grid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Contact_date_Grid.Name = "Contact_date_Grid";
            this.Contact_date_Grid.RowHeadersWidth = 62;
            this.Contact_date_Grid.Size = new System.Drawing.Size(611, 612);
            this.Contact_date_Grid.TabIndex = 0;
            // 
            // button_edit_contact
            // 
            this.button_edit_contact.Location = new System.Drawing.Point(631, 55);
            this.button_edit_contact.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button_edit_contact.Name = "button_edit_contact";
            this.button_edit_contact.Size = new System.Drawing.Size(167, 35);
            this.button_edit_contact.TabIndex = 10;
            this.button_edit_contact.Text = "изменить контакт";
            this.button_edit_contact.UseVisualStyleBackColor = true;
            this.button_edit_contact.Click += new System.EventHandler(this.button_edit_contact_Click);
            // 
            // buttom_add_contact
            // 
            this.buttom_add_contact.Location = new System.Drawing.Point(631, 10);
            this.buttom_add_contact.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttom_add_contact.Name = "buttom_add_contact";
            this.buttom_add_contact.Size = new System.Drawing.Size(167, 35);
            this.buttom_add_contact.TabIndex = 9;
            this.buttom_add_contact.Text = "добавить контакт";
            this.buttom_add_contact.UseVisualStyleBackColor = true;
            this.buttom_add_contact.Click += new System.EventHandler(this.buttom_add_contact_Click);
            // 
            // provider_table
            // 
            this.provider_table.BackColor = System.Drawing.Color.Pink;
            this.provider_table.Controls.Add(this.button_add_provider);
            this.provider_table.Controls.Add(this.button_delete_provider);
            this.provider_table.Controls.Add(this.Provider_date_Grid);
            this.provider_table.Location = new System.Drawing.Point(4, 29);
            this.provider_table.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.provider_table.Name = "provider_table";
            this.provider_table.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.provider_table.Size = new System.Drawing.Size(1246, 575);
            this.provider_table.TabIndex = 1;
            this.provider_table.Text = "Провайдер";
            // 
            // button_add_provider
            // 
            this.button_add_provider.Location = new System.Drawing.Point(411, 57);
            this.button_add_provider.Name = "button_add_provider";
            this.button_add_provider.Size = new System.Drawing.Size(206, 42);
            this.button_add_provider.TabIndex = 2;
            this.button_add_provider.Text = "Добавить провайдер";
            this.button_add_provider.UseVisualStyleBackColor = true;
            this.button_add_provider.Click += new System.EventHandler(this.button_add_provider_Click);
            // 
            // button_delete_provider
            // 
            this.button_delete_provider.Location = new System.Drawing.Point(411, 9);
            this.button_delete_provider.Name = "button_delete_provider";
            this.button_delete_provider.Size = new System.Drawing.Size(206, 42);
            this.button_delete_provider.TabIndex = 1;
            this.button_delete_provider.Text = "Удалить провайдер";
            this.button_delete_provider.UseVisualStyleBackColor = true;
            this.button_delete_provider.Click += new System.EventHandler(this.button_delete_provider_Click);
            // 
            // Provider_date_Grid
            // 
            this.Provider_date_Grid.BackgroundColor = System.Drawing.Color.Pink;
            this.Provider_date_Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Provider_date_Grid.Location = new System.Drawing.Point(10, 9);
            this.Provider_date_Grid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Provider_date_Grid.Name = "Provider_date_Grid";
            this.Provider_date_Grid.RowHeadersWidth = 62;
            this.Provider_date_Grid.Size = new System.Drawing.Size(374, 297);
            this.Provider_date_Grid.TabIndex = 0;
            // 
            // Telephone_table
            // 
            this.Telephone_table.BackColor = System.Drawing.Color.LemonChiffon;
            this.Telephone_table.Controls.Add(this.button2);
            this.Telephone_table.Controls.Add(this.button_editClient);
            this.Telephone_table.Controls.Add(this.button_addClient);
            this.Telephone_table.Controls.Add(this.Phone_number_date_Grid);
            this.Telephone_table.Location = new System.Drawing.Point(4, 29);
            this.Telephone_table.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Telephone_table.Name = "Telephone_table";
            this.Telephone_table.Size = new System.Drawing.Size(1246, 575);
            this.Telephone_table.TabIndex = 2;
            this.Telephone_table.Text = "Тел.справочник";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1077, 525);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(166, 36);
            this.button2.TabIndex = 3;
            this.button2.Text = "Удалить запись";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button_editClient
            // 
            this.button_editClient.Location = new System.Drawing.Point(905, 525);
            this.button_editClient.Name = "button_editClient";
            this.button_editClient.Size = new System.Drawing.Size(166, 36);
            this.button_editClient.TabIndex = 2;
            this.button_editClient.Text = "Изменить запись";
            this.button_editClient.UseVisualStyleBackColor = true;
            this.button_editClient.Click += new System.EventHandler(this.button_editClient_Click);
            // 
            // button_addClient
            // 
            this.button_addClient.Location = new System.Drawing.Point(733, 525);
            this.button_addClient.Name = "button_addClient";
            this.button_addClient.Size = new System.Drawing.Size(166, 36);
            this.button_addClient.TabIndex = 1;
            this.button_addClient.Text = "Добавить запись";
            this.button_addClient.UseVisualStyleBackColor = true;
            this.button_addClient.Click += new System.EventHandler(this.button_addClient_Click);
            // 
            // Phone_number_date_Grid
            // 
            this.Phone_number_date_Grid.BackgroundColor = System.Drawing.Color.LemonChiffon;
            this.Phone_number_date_Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Phone_number_date_Grid.GridColor = System.Drawing.SystemColors.ScrollBar;
            this.Phone_number_date_Grid.Location = new System.Drawing.Point(9, 8);
            this.Phone_number_date_Grid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Phone_number_date_Grid.Name = "Phone_number_date_Grid";
            this.Phone_number_date_Grid.RowHeadersWidth = 62;
            this.Phone_number_date_Grid.Size = new System.Drawing.Size(1025, 495);
            this.Phone_number_date_Grid.TabIndex = 0;
            // 
            // Number_phone
            // 
            this.Number_phone.Location = new System.Drawing.Point(30, 654);
            this.Number_phone.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Number_phone.Name = "Number_phone";
            this.Number_phone.Size = new System.Drawing.Size(313, 26);
            this.Number_phone.TabIndex = 2;
            this.Number_phone.TextChanged += new System.EventHandler(this.Number_phone_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 629);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Номер телефона";
            // 
            // FIO
            // 
            this.FIO.Location = new System.Drawing.Point(399, 654);
            this.FIO.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.FIO.Name = "FIO";
            this.FIO.Size = new System.Drawing.Size(450, 26);
            this.FIO.TabIndex = 4;
            this.FIO.TextChanged += new System.EventHandler(this.FIO_TextChanged);
            // 
            // ФИО
            // 
            this.ФИО.AutoSize = true;
            this.ФИО.Location = new System.Drawing.Point(425, 629);
            this.ФИО.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ФИО.Name = "ФИО";
            this.ФИО.Size = new System.Drawing.Size(47, 20);
            this.ФИО.TabIndex = 5;
            this.ФИО.Text = "ФИО";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1327, 689);
            this.Controls.Add(this.ФИО);
            this.Controls.Add(this.FIO);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Number_phone);
            this.Controls.Add(this.main_TableControl);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "Телефонный справочник";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.main_TableControl.ResumeLayout(false);
            this.Abonent_tab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Abonent_date_Grid)).EndInit();
            this.contact_table.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Contact_date_Grid)).EndInit();
            this.provider_table.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Provider_date_Grid)).EndInit();
            this.Telephone_table.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Phone_number_date_Grid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabControl main_TableControl;
        private System.Windows.Forms.TabPage Abonent_tab;
        private System.Windows.Forms.TabPage contact_table;
        private System.Windows.Forms.TabPage provider_table;
        private System.Windows.Forms.TabPage Telephone_table;
        private System.Windows.Forms.DataGridView Abonent_date_Grid;
        private System.Windows.Forms.DataGridView Contact_date_Grid;
        private System.Windows.Forms.DataGridView Provider_date_Grid;
        private System.Windows.Forms.DataGridView Phone_number_date_Grid;
        private System.Windows.Forms.TextBox Number_phone;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FIO;
        private System.Windows.Forms.Label ФИО;
        private System.Windows.Forms.Button button_add;
        private System.Windows.Forms.Button edit_buttom;
        private System.Windows.Forms.Button Delete_buttom;
        private System.Windows.Forms.Button buttom_add_contact;
        private System.Windows.Forms.Button button_edit_contact;
        private System.Windows.Forms.Button button_delete_contact;
        private System.Windows.Forms.Button button_delete_provider;
        private System.Windows.Forms.Button button_add_provider;
        private System.Windows.Forms.Button button_editClient;
        private System.Windows.Forms.Button button_addClient;
        private System.Windows.Forms.Button button2;
    }
}

