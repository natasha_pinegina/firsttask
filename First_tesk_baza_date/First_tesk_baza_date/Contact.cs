﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace First_tesk_baza_date
{
    public partial class Contact : Form
    {
        public Contact()
        {
            InitializeComponent();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
        public Dictionary<int, string> contact_Pinegina
        {
            set
            {
                comboBox_provider.DataSource = value.ToArray();
                comboBox_provider.DisplayMember = "Value";
            }
        }
        public int ProviderId
        {
            get
            {
                return ((KeyValuePair<int, string>)comboBox_provider.SelectedItem).Key;
            }
            set
            {
                int indx = 0;
                foreach (KeyValuePair<int, string> item in comboBox_provider.Items)
                {
                    if (item.Key == value)
                    {
                        break;
                    }
                    indx++;
                }
                comboBox_provider.SelectedIndex = indx;
            }
        }
    }
}
