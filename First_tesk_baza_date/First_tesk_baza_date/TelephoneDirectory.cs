﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace First_tesk_baza_date
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //private const string connectionString=" Data Source=192.168.9.5; Initial Catalog=KIS;Persist Security Info=True; User ID=sa; Password=sa";
        private const string connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\User\\Documents\\Программы\\First_task_baza_data\\first_task_basa_data\\First_tesk_baza_date\\First_tesk_baza_date\\Database2.mdf;Integrated Security=True";
        private void button1_Click(object sender, EventArgs e)
        {
            string query = "SELECT surname FROM abonent_Pinegina";
            SqlDataAdapter aOrder=new SqlDataAdapter(query,connectionString);
            DataTable table = new DataTable();
            aOrder.Fill(table);
            string res="";
            foreach(DataRow row in table.Rows)
            {
                res+=row["surname"].ToString()+"\n";
            }
            MessageBox.Show(res);
        }
        void updateAbonentDGV()
        {
            var request = "SELECT * FROM abonent_Pinegina";
            /*var request = @"SELECT abonent_Pinegina.birth_date, abonent_Pinegina.Id,abonent_Pinegina.surname, abonent_Pinegina.name,abonent_Pinegina.paitronymic, abonent_Pinegina.adress,abonent_Pinegina.comment, Provider_Pinegina.name_provider FROM
abonent_Pinegina JOIN abonent_has_contact_Pinegina ON abonent_Pinegina.Id = abonent_has_contact_Pinegina.abonent_id
 JOIN contact_Pinegina ON contact_Pinegina.Id = abonent_has_contact_Pinegina.contact_id
 LEFT JOIN Provider_Pinegina ON Provider_Pinegina.Id = contact_Pinegina.provider_key";*/
            var adapter = new SqlDataAdapter(request, connectionString);
            var abonent_table = new DataTable();
                adapter.Fill(abonent_table);
                Abonent_date_Grid.DataSource = abonent_table;
                Abonent_date_Grid.Columns["id"].Visible = false;
                Abonent_date_Grid.Columns["name"].HeaderText = "Имя";
                Abonent_date_Grid.Columns["surname"].HeaderText = "Фамилия";
                Abonent_date_Grid.Columns["paitronymic"].HeaderText = "Отчество";
                Abonent_date_Grid.Columns["adress"].HeaderText = "Адрес";
                Abonent_date_Grid.Columns["birth_date"].HeaderText = "Дата рождения";
                Abonent_date_Grid.Columns["comment"].HeaderText = "коментарий";

        }
        void updateContactDGV()
        {
            var request = "SELECT * FROM contact_Pinegina";
            var adapter = new SqlDataAdapter(request, connectionString);
            var abonent_table = new DataTable();
            adapter.Fill(abonent_table);
            Contact_date_Grid.DataSource = abonent_table;
            Contact_date_Grid.Columns["id"].Visible = false;
            Contact_date_Grid.Columns["phone"].HeaderText = "номер";
            Contact_date_Grid.Columns["type"].HeaderText = "тип";
            //Contact_date_Grid.Columns[""].HeaderText = "Отчество";

        }

        void updateProviderDGV()
        {
            var request = "SELECT * FROM Provider_Pinegina";
            var adapter = new SqlDataAdapter(request, connectionString);
            var abonent_table = new DataTable();
            adapter.Fill(abonent_table);
            Provider_date_Grid.DataSource = abonent_table;
            Provider_date_Grid.Columns["id"].Visible = false;
            Provider_date_Grid.Columns["name_provider"].HeaderText = "название";
            Provider_date_Grid.Columns["score"].HeaderText = "оценка качества";
        }

        void updateDGV()
        {
            //            var request = @"SELECT abonent_Pinegina.surname, abonent_Pinegina.name,Provider_Pinegina.name_provider, contact_Pinegina.phone FROM
            //abonent_Pinegina JOIN abonent_has_contact_Pinegina ON abonent_Pinegina.Id=abonent_has_contact_Pinegina.abonent_id
            // JOIN contact_Pinegina ON contact_Pinegina.Id=abonent_has_contact_Pinegina.contact_id 
            // LEFT JOIN Provider_Pinegina ON Provider_Pinegina.Id=contact_Pinegina.provider_key ";
            var request = @"SELECT * FROM abonent_Pinegina JOIN abonent_has_contact_Pinegina
                                ON abonent_Pinegina.Id=abonent_has_contact_Pinegina.abonent_id
                            JOIN contact_Pinegina
                                ON contact_Pinegina.Id=abonent_has_contact_Pinegina.contact_id
                                LEFT JOIN Provider_Pinegina
                                ON Provider_Pinegina.Id=contact_Pinegina.provider_key
                                ";
            //if (Number_phone.Text != "")
            //{
            //    request +=@"WHERE contact_Pinegina.phone  LIKE '%" + Number_phone.Text + "%'";
            //}
            //if(FIO.Text !="")
            //{
            //    request += @"WHERE abonent_Pinegina.surname+' ' + abonent_Pinegina.name+' '+abonent_Pinegina.paitronymic LIKE '%" + FIO.Text + "%'";
            //}
            request += @"WHERE contact_Pinegina.phone  LIKE '%" + Number_phone.Text + "%' AND abonent_Pinegina.surname+' ' + abonent_Pinegina.name+' '+abonent_Pinegina.paitronymic LIKE '%" + FIO.Text + "%' ";
            var adapter = new SqlDataAdapter(request, connectionString);
            var abonent_table = new DataTable();
            adapter.Fill(abonent_table);
            Phone_number_date_Grid.DataSource = abonent_table;
            Phone_number_date_Grid.Columns["Id"].Visible = false;

            Phone_number_date_Grid.Columns["contact_id"].Visible = false;
            Phone_number_date_Grid.Columns["birth_date"].Visible = false;
            Phone_number_date_Grid.Columns["comment"].Visible = false;
            Phone_number_date_Grid.Columns["abonent_id"].Visible = false;
            Phone_number_date_Grid.Columns["Id1"].Visible = false;
            Phone_number_date_Grid.Columns["Id2"].Visible = false;
            Phone_number_date_Grid.Columns["score"].Visible = false;
            Phone_number_date_Grid.Columns["provider_key"].Visible = false;

            Phone_number_date_Grid.Columns["surname"].HeaderText = "фамилия";
            Phone_number_date_Grid.Columns["name"].HeaderText = "имя";
            Phone_number_date_Grid.Columns["name_provider"].HeaderText = "название провайдера";
            Phone_number_date_Grid.Columns["phone"].HeaderText = "номер телефона";
            Phone_number_date_Grid.Columns["paitronymic"].HeaderText = "отчество";
            Phone_number_date_Grid.Columns["adress"].HeaderText = "адрес";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            updateAbonentDGV();
            updateContactDGV();
            updateProviderDGV();
            updateDGV();
        }

        private void Number_phone_TextChanged(object sender, EventArgs e)
        {
            updateDGV();
        }

        private void FIO_TextChanged(object sender, EventArgs e)
        {
            updateDGV();
        }

        private void button_add_Click(object sender, EventArgs e)
        {
            var form = new Abonent();
            var res = form.ShowDialog();

            if (res == DialogResult.OK)
            {
                var surname = form.surname_TextBox.Text;
                var name = form.name_TextBox.Text;
                var patronymic = form.paitronymic_TextBox.Text;
                var comment = form.comment_TextBox.Text; ;
                var adress = form.adress_TextBox.Text;
                var connection = new SqlConnection(connectionString);
                connection.Open();
                var request = @"INSERT INTO abonent_Pinegina (name,surname,paitronymic,comment,adress,birth_date)
                VALUES ('" + name+ "','" + surname + "','" + patronymic + "','" + comment+ "','" + adress + "','02.05.1998')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updateAbonentDGV();
            }
        }

        private void edit_buttom_Click(object sender, EventArgs e)
        {
            var row = Abonent_date_Grid.SelectedRows.Count > 0 ? Abonent_date_Grid.SelectedRows[0] : null;
            if(row==null)
            {
                MessageBox.Show("Сначала выбирете строку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new Abonent();
            form.surname_TextBox.Text = row.Cells["surname"].Value.ToString();
            form.name_TextBox.Text = row.Cells["name"].Value.ToString();
            form.paitronymic_TextBox.Text = row.Cells["paitronymic"].Value.ToString();
            form.comment_TextBox.Text = row.Cells["comment"].Value.ToString();
            form.adress_TextBox.Text = row.Cells["adress"].Value.ToString();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var surname = form.surname_TextBox.Text;
                var name = form.name_TextBox.Text;
                var patronymic = form.paitronymic_TextBox.Text;
                var comment = form.comment_TextBox.Text; ;
                var adress = form.adress_TextBox.Text;
                var id = row.Cells["id"].Value.ToString();
                var connection = new SqlConnection(connectionString);
                connection.Open();
                var request = @"UPDATE abonent_Pinegina SET surname='" + surname + "',name='" + name + "',paitronymic = '" + patronymic + "', adress ='" + adress + "', comment='" + comment + "'WHERE Id='" + id + "';";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updateAbonentDGV();
            }
        }

        private void Delete_buttom_Click(object sender, EventArgs e)
        {
            var row = Abonent_date_Grid.SelectedRows.Count > 0 ? Abonent_date_Grid.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выбирете строку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = row.Cells["id"].Value.ToString();
            var connection = new SqlConnection(connectionString);
            connection.Open();
            var request = @"DELETE FROM abonent_Pinegina WHERE Id='" + id + "';";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery();
            connection.Close();
            updateAbonentDGV();
        }

        private void buttom_add_contact_Click(object sender, EventArgs e)
        {
            var form = new Contact();
            {
                var request = @"SELECT * FROM Provider_Pinegina";
                var adapter = new SqlDataAdapter(request, connectionString);
                var orgTable = new DataTable();
                adapter.Fill(orgTable);
                var dict = new Dictionary<int, string>();
                foreach (DataRow row in orgTable.Rows)
                {
                    dict.Add((int)row["Id"], row["name_provider"].ToString());
                }
                form.contact_Pinegina = dict;
            }
            var res = form.ShowDialog();

            if (res == DialogResult.OK)
            {
                var phone = form.phone_TextBox.Text;
                var type = form.type_textBox.Text;
                var connection = new SqlConnection(connectionString);
                var providerId = form.ProviderId;
                connection.Open();
                var request = @"INSERT INTO contact_Pinegina (phone,type,provider_key)
                VALUES ('" + phone + "','" + type + "','" + providerId.ToString() + "');";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updateContactDGV();
            }
        }

        private void button_edit_contact_Click(object sender, EventArgs e)
        {
            var row = Contact_date_Grid.SelectedRows.Count > 0 ? Contact_date_Grid.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выбирете строку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new Contact();
            form.phone_TextBox.Text = row.Cells["phone"].Value.ToString();
            form.type_textBox.Text = row.Cells["type"].Value.ToString();
            {
                var request = "SELECT * FROM Provider_Pinegina";
                var adapter = new SqlDataAdapter(request, connectionString);
                var providerTable = new DataTable();
                adapter.Fill(providerTable);
                var dict = new Dictionary<int, string>();
                foreach (DataRow dbRow in providerTable.Rows)
                {
                    dict.Add((int)dbRow["Id"], dbRow["name_provider"].ToString());
                }
                form.contact_Pinegina = dict;
            }
            
            form.ProviderId = (int)row.Cells["provider_key"].Value;

            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var phone = form.phone_TextBox.Text;
                var type = form.type_textBox.Text;
                var id = row.Cells["Id"].Value.ToString();
                var providerId = form.ProviderId;
                var connection = new SqlConnection(connectionString);
                connection.Open();
                var request = @"UPDATE contact_Pinegina SET phone='" + phone + "',type='" + type + "', provider_key='" + providerId.ToString() + "' WHERE Id='" + id + "';";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updateContactDGV();
            }
        }

        private void button_delete_contact_Click(object sender, EventArgs e)
        {
            var row = Contact_date_Grid.SelectedRows.Count > 0 ? Contact_date_Grid.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выбирете строку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = row.Cells["Id"].Value.ToString();
            var connection = new SqlConnection(connectionString);
            connection.Open();
            var request = @"DELETE FROM contact_Pinegina WHERE Id='" + id + "';";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery();
            connection.Close();
            updateContactDGV();
        }

        private void button_delete_provider_Click(object sender, EventArgs e)
        {
            var row = Provider_date_Grid.SelectedRows.Count > 0 ? Provider_date_Grid.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выбирете строку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = row.Cells["Id"].Value.ToString();
            var connection = new SqlConnection(connectionString);
            connection.Open();
            var request = @"DELETE FROM Provider_Pinegina WHERE Id='" + id + "';";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery();
            connection.Close();
            updateProviderDGV();
        }

        private void button_add_provider_Click(object sender, EventArgs e)
        {
            var form = new Provider();
            {
                var request = @"SELECT * FROM Provider_Pinegina";
                var adapter = new SqlDataAdapter(request, connectionString);
                var orgTable = new DataTable();
                adapter.Fill(orgTable);
                var dict = new Dictionary<int, int>();
                foreach (DataRow row in orgTable.Rows)
                {
                    dict.Add((int)row["Id"], (int)row["score"]);
                }
                form.ProviderPinegina = dict;
            }
            var res = form.ShowDialog();

            if (res == DialogResult.OK)
            {
                //var score = form.Provider_TextBox.Text;
                var provider = form.Provider_TextBox.Text;
                var connection = new SqlConnection(connectionString);
                var providerId = form.ProviderId;
                connection.Open();
                var request = @"INSERT INTO Provider_Pinegina (name_provider,score)
                VALUES ('" + provider + "','" + providerId.ToString() + "');";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updateProviderDGV();
            }
        }

        private void button_addClient_Click(object sender, EventArgs e)
        {
            var form = new theEntryintheDirectory();
            {
                var request = @"SELECT Id,name+' '+surname+' '+paitronymic+' ' AS FIO FROM abonent_Pinegina";
                var adapter = new SqlDataAdapter(request, connectionString);
                var orgTable = new DataTable();
                adapter.Fill(orgTable);
                var dict = new Dictionary<int, string>();
                foreach (DataRow row in orgTable.Rows)
                {
                    dict.Add((int)row["Id"], row["FIO"].ToString());
                }
                form.PersonPinegina = dict;
            }
            {
                var request = @"SELECT Id,phone+' ' AS count FROM contact_Pinegina";
                var adapter = new SqlDataAdapter(request, connectionString);
                var orgTable = new DataTable();
                adapter.Fill(orgTable);
                var dict = new Dictionary<int, string>();
                foreach (DataRow row in orgTable.Rows)
                {
                    dict.Add((int)row["Id"], row["count"].ToString());
                }
                form.PhonePinegina = dict;
            }
            var res = form.ShowDialog();

            if (res == DialogResult.OK)
            {
                var person = form.PersonId;
                var phone = form.PhoneId;
                var connection = new SqlConnection(connectionString);
                //var providerId = form.ProviderId;
                connection.Open();
                var request = @"INSERT INTO abonent_has_contact_Pinegina(abonent_id,contact_id)
                VALUES ('" + person.ToString() + "','"+phone.ToString()+"');";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updateDGV();
            }
        }

        private void button_editClient_Click(object sender, EventArgs e)
        {
            var row = Phone_number_date_Grid.SelectedRows.Count > 0 ? Phone_number_date_Grid.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выбирете строку", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            var form = new theEntryintheDirectory();
            {
                var request = "SELECT Id, surname + ' ' + name + ' ' + paitronymic AS fio FROM abonent_Pinegina";
                var adapter = new SqlDataAdapter(request, connectionString);
                var abonTable = new DataTable();
                adapter.Fill(abonTable);
                var dict = new Dictionary<int, string>();
                foreach (DataRow dbrow in abonTable.Rows)
                {
                    dict.Add((int)dbrow["Id"], dbrow["fio"].ToString());
                }
                form.PersonPinegina = dict;
            }

            {
                var request = "SELECT Id, phone + ' (' + type + ' )'  AS cont FROM contact_Pinegina";
                var adapter = new SqlDataAdapter(request, connectionString);
                var contTable = new DataTable();
                adapter.Fill(contTable);
                var dict = new Dictionary<int, string>();
                foreach (DataRow dbrow in contTable.Rows)
                {
                    dict.Add((int)dbrow["Id"], dbrow["cont"].ToString());
                }
                form.PhonePinegina = dict;
            }

            form.PersonId = (int)row.Cells["abonent_id"].Value;
            form.PhoneId = (int)row.Cells["contact_id"].Value;

            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                
                var abonentid = row.Cells["abonent_id"].Value.ToString();
                var contactid = row.Cells["contact_id"].Value.ToString();
                var abonent = form.PersonId;
                var contact = form.PhoneId;
                var connection = new SqlConnection(connectionString);
                connection.Open();
                var request = @"UPDATE abonent_has_contact_Pinegina SET 
                        contact_id='" + contact.ToString() + "', abonent_id='" + abonent.ToString() + "' " +
                       "WHERE contact_id=" + contactid.ToString() + " AND abonent_id=" + abonentid.ToString() + "";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updateDGV();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var row = Phone_number_date_Grid.SelectedRows.Count > 0 ? Phone_number_date_Grid.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var contact = (int)row.Cells["contact_id"].Value;
            var abonent = (int)row.Cells["abonent_id"].Value;
            var connection = new SqlConnection(connectionString);
            connection.Open();
            var request = "DELETE FROM abonent_has_contact_Pinegina WHERE contact_id=" + contact.ToString() + "AND abonent_id=" + abonent.ToString() + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery();
            connection.Close();
            updateDGV();
        }
    }
}
